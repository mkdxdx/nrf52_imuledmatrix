#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "nrfx_spim.h"
#include "nrfx_twim.h"
#include "nrf_delay.h"
#include "ledMatrix.h"
#include "imu.h"

static void initSpim(void);
static void initI2c(void);
static void initMatrix(void);
static void initImu(void);
static void readImu(void);
static void matrixRefresh(void);
static void matrixClear(void);
static void matrixPoint(uint8_t x, uint8_t y, bool set);
static void drawGraph(void);
static void solveCollision(uint8_t idx);

typedef struct Vec {
    int16_t x;
    int16_t y;
} Vec;

static const uint32_t PinI2cSda = 23;
static const uint32_t PinI2cScl = 22;
static const uint32_t PinSpiMosi = 27;
static const uint32_t PinSpiCs = 28;
static const uint32_t PinSpiSck = 29;
static const uint32_t Scale = 20;
static const uint32_t Size = 20;

static const nrfx_spim_t spim = NRFX_SPIM_INSTANCE(0);
static const nrfx_twim_t twim = NRFX_TWIM_INSTANCE(1);

static uint8_t matrixData[8] = {0};
static int8_t accelX,accelY,accelZ;
#define POINT_COUNT 3
struct {
    Vec pos;
    Vec delta;
} points[POINT_COUNT] = {
    { .pos = {.x = 50, .y = 25}, .delta = {.x = 10, .y = 10} },
    { .pos = {.x = 50, .y = 50}, .delta = {.x = 10, .y = 5} },
    { .pos = {.x = 25, .y = 10}, .delta = {.x = -10, .y = 5} },
};


int main(void)
{
    initSpim();
    initI2c();
    initMatrix();
    initImu();

    while (true)
    {
        static uint32_t framesElapsed = 0;
        readImu();
        drawGraph();
        nrf_delay_ms(50);
        if (++framesElapsed > 10) {
            framesElapsed = 0;
            for (uint32_t i = 0; i < POINT_COUNT; i++) {
                if (points[i].delta.x != 0) {
                    points[i].delta.x = points[i].delta.x > 0 ? points[i].delta.x - 1 : points[i].delta.x + 1;
                }
                if (points[i].delta.y != 0) {
                    points[i].delta.y = points[i].delta.y > 0 ? points[i].delta.y - 1 : points[i].delta.y + 1;
                }
            }
        }
    }
}

static void initSpim(void)
{
    nrfx_spim_config_t config = {
        .sck_pin        = PinSpiSck,
        .mosi_pin       = PinSpiMosi,
        .miso_pin       = NRFX_SPIM_PIN_NOT_USED,
        .ss_pin         = PinSpiCs,
        .ss_active_high = false,
        .irq_priority   = NRFX_SPIM_DEFAULT_CONFIG_IRQ_PRIORITY,
        .orc            = 0xFF,
        .frequency      = NRF_SPIM_FREQ_1M,
        .mode           = NRF_SPIM_MODE_0,
        .bit_order      = NRF_SPIM_BIT_ORDER_MSB_FIRST,
    };
    nrfx_err_t ret = nrfx_spim_init(&spim, &config, NULL, NULL);
}

static void initI2c(void)
{
    nrfx_twim_config_t config = {
        .scl = PinI2cScl,
        .sda = PinI2cSda,
        .frequency = NRF_TWIM_FREQ_400K,
        .interrupt_priority = NRFX_TWIM_DEFAULT_CONFIG_IRQ_PRIORITY,
        .hold_bus_uninit = NRFX_TWIM_DEFAULT_CONFIG_HOLD_BUS_UNINIT
    };

    nrfx_err_t ret = nrfx_twim_init(&twim, &config, NULL, NULL);
    nrfx_twim_enable(&twim);
}

static void initMatrix(void)
{
    ledMatrixInit();
    ledMatrixTest(true);
    nrf_delay_ms(1000);
    ledMatrixTest(false);

    for (uint32_t i = 0; i < 8; i++) {
        ledMatrixWriteRow(i, 0);
    }
}

static void initImu(void)
{
    imuInit();
}

static void readImu(void)
{
    imuReadG(&accelX, &accelY, &accelZ);
}

static void matrixRefresh(void)
{
    for (uint32_t i = 0; i < 8; i++) {
        ledMatrixWriteRow(i, matrixData[i]);
    }
}

static void matrixPoint(uint8_t x, uint8_t y, bool set)
{
    if (x >= 8 || y >= 8) {
        return;
    }
    matrixData[y]= set
                    ? matrixData[y] | (1<<(7-x))
                    : matrixData[y] & (~(1<<(7-x)));
}

static void matrixClear(void)
{
    memset(matrixData, 0, sizeof(matrixData));
}

static void drawGraph(void)
{
    matrixClear();

    for (uint32_t i = 0; i < POINT_COUNT; i++) {
        if (accelX > 64 || accelX < -64) {
            points[i].delta.x += accelX  / 10;
        }
        if (accelY > 64 || accelY < -64) {
            points[i].delta.y += accelY / 10;
        }
        solveCollision(i);
        points[i].pos.x += points[i].delta.x;
        points[i].pos.y += points[i].delta.y;
    }
    for (uint32_t i = 0; i < POINT_COUNT; i++) {
        uint32_t px = (uint32_t)points[i].pos.x;
        uint32_t py = (uint32_t)points[i].pos.y;
        for (uint32_t j = 0; j < Size; j++) {
            matrixPoint((px + j) / Scale , py / Scale, true);
            matrixPoint((px + j) / Scale , (py + Size) / Scale, true);
        }
        for (uint32_t j = 0; j < Size; j++) {
            matrixPoint(px / Scale , (py + j) / Scale, true);
            matrixPoint((px + Size) / Scale , (py + j) / Scale, true);
        }
    }
    matrixRefresh();
}

static void solveCollision(uint8_t idx)
{
    bool swapX = false;
    bool swapY = false;

    int16_t px = points[idx].pos.x;
    int16_t py = points[idx].pos.y;
    int16_t dx = points[idx].delta.x;
    int16_t dy = points[idx].delta.y;

    swapX |= (px + Size + dx) >= 8 * Scale || px + dx < 0;
    swapY |= (py + Size + dy) >= 8 * Scale || py + dy < 0;

    if (swapX) {
        dx = dx * -1;
        points[idx].delta.x = dx;
    }
    if (swapY) {
        dy = dy * -1;
        points[idx].delta.y = dy;
    }

}

void ledMatrixDataWrite(uint8_t *data, uint32_t length)
{
    const nrfx_spim_xfer_desc_t xfer = NRFX_SPIM_XFER_TX(data, length);
    nrfx_err_t ret = nrfx_spim_xfer(&spim, &xfer, 0);
}

void imuDataWrite(const uint8_t *data, uint32_t length)
{
    nrfx_twim_xfer_desc_t xfer = NRFX_TWIM_XFER_DESC_TX(IMU_ADDRESS, data, length);
    nrfx_err_t err = nrfx_twim_xfer(&twim, &xfer, 0);
}

void imuDataRead(uint8_t reg, uint8_t *rxData, uint32_t rxLength)
{
    nrfx_twim_xfer_desc_t xfer = NRFX_TWIM_XFER_DESC_TXRX(IMU_ADDRESS, &reg, sizeof(reg), rxData, rxLength);
    nrfx_err_t err = nrfx_twim_xfer(&twim, &xfer, 0);
}
