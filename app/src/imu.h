#pragma once

#include <stdint.h>
#include <stdbool.h>

// address is 7 bits
#define IMU_ADDRESS (0x1C)

void imuInit(void);
void imuReadG(int8_t *x, int8_t *y, int8_t *z);

extern void imuDataWrite(const uint8_t *data, uint32_t length);
extern void imuDataRead(uint8_t reg, uint8_t *rxData, uint32_t rxLength);
