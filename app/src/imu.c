#include <stdint.h>
#include <stdbool.h>
#include "imu.h"

enum Reg {
    RegStatus = 0x00,
    RegOutXMsb = 0x01,
    RegOutXLsb = 0x02,
    RegOutYMsb = 0x03,
    RegOutYLsb = 0x04,
    RegOutZMsb = 0x05,
    RegOutZLsb = 0x06,
    RegSysMod  = 0x0B,
    RegWhoAmI  = 0x0D,
    RegXyzDataCfg = 0x0E,
    RegAslpCount = 0x29,
    RegCtrl1 = 0x2A,
    RegCtrl2 = 0x2B,
    RegCtrl3 = 0x2C,
    RegCtrl4 = 0x2D,
    RegCtrl5 = 0x2E
};

static void regWrite(uint8_t reg, uint8_t value);
static uint8_t regRead(uint8_t reg);

void imuInit(void)
{
    uint8_t data = regRead(RegWhoAmI);
    if (data != 0x2A) {
        // it is not MMA8452
        return;
    }

    // device reset
    data = 1<<6;
    regWrite(RegCtrl2, data);
    for (uint32_t i = 0; i < 0x3FF; i++) {}

    // active mode enabled, default data rate, fast read mode (8bit MSB)
    data = 1<<0 | 1<<1;
    regWrite(RegCtrl1, data);

    // high pass enabled, FS1:FS0 = 2g scale
    data = 1<<4;
    regWrite(RegXyzDataCfg, data);

}

void imuReadG(int8_t *x, int8_t *y, int8_t *z)
{
    *x = (int8_t)regRead(RegOutXMsb);
    *y = (int8_t)regRead(RegOutYMsb);
    *z = (int8_t)regRead(RegOutZMsb);
}

static void regWrite(uint8_t reg, uint8_t value)
{
    uint8_t data[2] = {reg, value};
    imuDataWrite(data, sizeof(data));
}

static uint8_t regRead(uint8_t reg)
{
    uint8_t data = 0;
    imuDataRead(reg, &data, sizeof(data));
    return data;
}
