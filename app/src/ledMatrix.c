#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "ledMatrix.h"

enum Reg {
    RegNoOp,
    RegD0,
    RegD1,
    RegD2,
    RegD3,
    RegD4,
    RegD5,
    RegD6,
    RegD7,
    RegDecodeMode,
    RegIntensity,
    RegScanLimit,
    RegShutdown,
    RegDisplayTest = 0x0F
};

static void regWrite(uint8_t reg, uint8_t value);

void ledMatrixInit(void)
{
    // normal op
    regWrite(RegShutdown, 0x01);
    // intensity 5/32
    regWrite(RegIntensity, 0x02);
    // display all digits
    regWrite(RegScanLimit, 0x07);
    // no decode
    regWrite(RegDecodeMode, 0x00);
}

void ledMatrixTest(bool active)
{
    regWrite(RegDisplayTest, active ? 0x01 : 0x00);
}

void ledMatrixWriteRow(uint8_t row, uint8_t value)
{
    if (row >= 8) {
        return;
    }
    regWrite(RegD0 + row, value);
}

static void regWrite(uint8_t reg, uint8_t value)
{
    union {
        struct {
            uint8_t reg;
            uint8_t value;
        };
        uint8_t byte[2];
    } packet = {
        .reg = reg,
        .value = value
    };
    ledMatrixDataWrite(packet.byte, sizeof(packet.byte));
}
