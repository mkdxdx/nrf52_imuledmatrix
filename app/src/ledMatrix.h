#pragma once

#include <stdint.h>
#include <stdbool.h>

void ledMatrixInit(void);
void ledMatrixTest(bool active);
void ledMatrixWriteRow(uint8_t row, uint8_t value);

extern void ledMatrixDataWrite(uint8_t *data, uint32_t length);
